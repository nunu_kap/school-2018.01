import Pyro4
from .answer_processor import AnswerProcessor


@Pyro4.expose
class ClientService:
    def __init__(self, master_uri):
        self.master_client = Pyro4.Proxy(master_uri)

    def search(self, query):
        answer_processor = AnswerProcessor(self.master_client.search(query))
        return answer_processor.build_answer()
